from ecdsa import SigningKey, SECP256k1

from src.block import Block
from src.merkle_tree import MerkleTree

def get_test_block() -> Block:
    """
    Method for getting a generic initial block for testing purposes.
    :return: A generic Block for testing purposes
    """
    return get_custom_block(91,"test")

def get_secondary_test_block() -> Block:
    return get_custom_block(880,get_test_block().hash)


def get_custom_block(proof:int,prev_hash:str) -> Block:
    """
    Method for getting a generic initial block for testing purposes, with custom prev_hash.
    :return: A generic Block for testing purposes
    """
    return Block(0,0,MerkleTree([]),proof,prev_hash)


def get_test_key():
    private_pem = b'\xd8\xbd\x18\xb4F\xa8\xea\x7fU\x11\xbd(:{\xb8\xda@\xc2\xfbBw +9\x99\xa1\xa5i\x8e\xb5\x99X'
    sk = SigningKey.from_string(private_pem, SECP256k1)
    return sk


def get_secondary_test_key():
    private_pem = b'ZZ8\xfc\xe1\xcen,\x9c\x83/\x94\x12\xf53\x94\x9b\xc8\xab\x14\x16t\xc2\xea\x80C\x99k\x19\xcc\xe9J'
    sk = SigningKey.from_string(private_pem, SECP256k1)
    return sk