import unittest

from src.referendum import Referendum
from src.merkle_tree import MerkleTree
from src.vote import Vote
from tests.utils import get_test_key


class TestMerkleRoot(unittest.TestCase):

    def setUp(self):
        ref = Referendum("more fish","do more fish?",get_test_key())
        self.data = [ref, Vote(ref.id, True,get_test_key()), Vote(ref.id, False,get_test_key())]

    def test_is_the_same(self):
        tree1 = MerkleTree(self.data)
        tree2 = MerkleTree(self.data)

        self.assertEqual(tree1.root,tree2.root)

    def test_gets_correct_root(self):
        tree = MerkleTree(self.data)
        self.assertEqual("5aa2eacacb553fbe66b5a8c11aa6230f0e08a3c7b822b438d56f5f8d38f5b5a3",tree.root)

    def test_different_is_different(self):
        tree1 = MerkleTree(self.data)
        tree2 = MerkleTree(self.data[:1])

        self.assertNotEqual(tree1.root,tree2.root)

    def test_generates_correct_hashes(self):
        tree1 = MerkleTree(self.data)

        #3 because there should be a fourth because of it being even.
        self.assertListEqual(tree1.bottom_layer()[:3],[d.id for d in self.data])

    def test_can_concat_and_regenerates(self):
        tree1 = MerkleTree(self.data)

        tree1.concat(self.data)

        self.assertListEqual(tree1.items,self.data*2)
        self.assertListEqual(tree1.bottom_layer(),[d.id for d in self.data*2])

    def test_layer_correct_length(self):
        tree1 = MerkleTree(self.data)

        #3 because there should be a fourth because of it being even.
        self.assertEqual(len(tree1.bottom_layer()),4)

    def test_one_big_is_same(self):
        tree = MerkleTree(self.data[:1])
        self.assertEqual(tree.root, self.data[0].id)

    def test_proofs_is_correct_structure(self):
        tree = MerkleTree(self.data)
        proofs = tree.get_proofs(1)

        self.assertFalse(sum(sum(1 if val is None else 0 for val in level)==0 for level in proofs))

    def test_valid_proof_is_valid(self):
        tree = MerkleTree(self.data)
        proofs = tree.get_proofs(1)
        self.assertTrue(MerkleTree.validate_proofs(proofs, self.data[1].id, tree.root))


    def test_works_with_uneven_amount(self):
        data = self.data*2
        tree = MerkleTree(data)
        proofs = tree.get_proofs(5)
        self.assertTrue(MerkleTree.validate_proofs(proofs, data[5].id, tree.root))


    def test_invalid_proof_is_invalid(self):
        tree = MerkleTree(self.data)
        proofs = tree.get_proofs(2)
        self.assertFalse(MerkleTree.validate_proofs(proofs, self.data[1].id, tree.root))
