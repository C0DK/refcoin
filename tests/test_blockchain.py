import unittest

from src.block import Block
from src.blockchain import Blockchain, ChainInvalidException
from src.merkle_tree import MerkleTree
from tests.utils import get_test_block, get_custom_block, get_secondary_test_block


class TestInitBlockchain(unittest.TestCase):
    """
    Test everything regarding initialization of the Blockchain class
    """
    def setUp(self):
        Block.DIFFICULTY = 1

    def test_can_init(self):
        Blockchain()

    def test_valid_chain_is_valid(self):
        block1 = get_test_block()
        block2 = get_secondary_test_block()
        Blockchain([block1, block2])

    def test_invalid_last_hash_valid_proof(self):
        block1 = get_test_block()
        # creates block with valid hash but invalid in regards to block1
        block2 = get_custom_block(127,"00a565d09938de3d21773274ce5f26241372bcf7818c9522c9b945c27d63c36d")

        with self.assertRaises(ChainInvalidException):
            Blockchain([block1, block2])

    def test_can_manually_add_block(self):
        block1 = get_test_block()
        block2 = get_secondary_test_block()
        chain = Blockchain([block1])
        chain.add(block2)

