import unittest

from src.block import Block, InvalidHashException
from tests.test_blockchain import get_test_block
from tests.utils import get_custom_block


class TestBlocks(unittest.TestCase):
    """
    Test everything regarding the Block class
    """
    def setUp(self):
        Block.DIFFICULTY = 1


    def test_can_init(self):
        try:
            get_test_block()
        except InvalidHashException:
            self.fail("invalid hash")

    def test_str(self):
        str(get_test_block())

    def test_cannot_init_invalid(self):
        with self.assertRaises(InvalidHashException):
            get_custom_block(1,"fisk")

    def test_correct_hash(self):
        block = get_test_block()
        self.assertEqual(block.hash,"00a565d09938de3d21773274ce5f26241372bcf7818c9222c9b945c27d63c36d")
