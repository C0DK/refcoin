import json
import unittest

from ecdsa import VerifyingKey, BadSignatureError

from src.iblock_data_item import IBlockDataItem
from src.jsonable import IJsonable
from src.referendum import Referendum
from src.vote import Vote
from tests.utils import get_test_key, get_secondary_test_key


class TestBlockData(unittest.TestCase):
    """
    Test everything regarding the Block data class
    """

    def test_referendum_get_txids(self):
        referendum1 = Referendum("test1","body",get_test_key())
        referendum2 = Referendum("test2","body",get_test_key())

        self.assertNotEqual(referendum1,referendum2)

    def test_vote_get_txids(self):
        referendum1 = Referendum("test1","body",get_test_key())
        vote1 = Vote(referendum1.id, True,get_test_key())
        vote2 = Vote(referendum1.id, False,get_test_key())

        self.assertNotEqual(vote1,vote2)

    def test_referendum_has_correct_json(self):
        referendum = Referendum("test1","body",get_test_key())
        data = referendum.to_json()
        self.assertEqual(data["title"],referendum.title)
        self.assertEqual(data["body"],referendum.body)
        self.assertEqual(data["id"], referendum.id)
        self.assertEqual(data["creator_key"], referendum.creator_key.to_string())
        self.assertEqual(data["signature"], referendum.signature)

    def test_vote_has_correct_json(self):
        vote = Vote("test1",False,get_test_key())
        data = vote.to_json()
        self.assertEqual(data["ref_id"],vote.referendum_id)
        self.assertEqual(data["value"],vote.value)
        self.assertEqual(data["id"], vote.id)
        self.assertEqual(data["creator_key"], vote.creator_key.to_string())
        self.assertEqual(data["signature"], vote.signature)

    def test_ijsonable_raises_not_implemented(self):
        with self.assertRaises(TypeError):
            IJsonable()


    def test_dataitem_raises_not_implemented(self):
        with self.assertRaises(TypeError):
            IBlockDataItem("1", get_test_key())

    def test_is_correctly_signed(self):
        obj = Referendum("test","body",get_test_key())
        self.assertTrue(obj.creator_key.verify(obj.signature, obj.id.encode('utf-8')))

    def test_fails_on_incorrect(self):
        obj = Referendum("test","body",get_test_key())
        obj.creator_key = get_secondary_test_key().get_verifying_key()
        with self.assertRaises(BadSignatureError):
            self.assertFalse(obj.creator_key.verify(obj.signature, obj.id.encode('utf-8')))