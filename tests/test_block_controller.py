import unittest
from threading import Thread
from time import sleep

from ecdsa import BadSignatureError

from src.block import Block
from src.block_controller import BlockController, NewChainIsShorterThanOldException, ElementInvalidException, \
    DoubleVoteException, ReferendumNotFoundException
from src.referendum import Referendum
from src.vote import Vote
from tests.utils import get_test_key, get_secondary_test_key


class TestBlockController(unittest.TestCase):
    def setUp(self):
        Block.DIFFICULTY = 1

    def test_can_mine(self):
        block_controller = BlockController()

        block = block_controller.mine_next_block()

        self.assertTrue(block.is_valid())

    def test_stops_on_stop_flag(self):
        block_controller = BlockController()

        thread = Thread(target=block_controller.continuously_mine)
        thread.start()
        self.assertTrue(thread.is_alive())
        block_controller.miner_stop_flag = True
        sleep(0.2)  # wait some time for it to stop
        self.assertFalse(thread.is_alive())

    def test_continuously_mines(self):
        block_controller = BlockController()

        thread = Thread(target=block_controller.continuously_mine)
        thread.start()
        sleep(0.1)
        # controller crashes if data is None
        block_controller.miner_stop_flag = True
        self.assertGreater(len(block_controller.blockchain), 2)


    def test_multiple_is_added(self):
        block_controller = BlockController()
        amount = 5
        for i in range(amount):
            block_controller.add(Referendum(f"ref #{i}","pants",get_test_key()))
        block_controller.mine_next_block()

        self.assertEqual(len(block_controller.blockchain.head.data),amount)

    def test_removes_when_mined(self):
        block_controller = BlockController()
        for i in range(5):
            block_controller.add(Referendum(f"ref #{i}","pants",get_test_key()))
        block_controller.mine_next_block()
        block_controller.add(Referendum("ref extra","pants",get_test_key()))

        self.assertEqual(len(block_controller.merkle_tree.items),1)

    def test_cannot_add_smaller(self):
        block_controller = BlockController()
        for i in range(5):
            block_controller.add(Referendum(f"ref #{i}","pants",get_test_key()))
        block_controller.mine_next_block()

        with self.assertRaises(NewChainIsShorterThanOldException):
            block_controller.set_chain([])


    def test_is_valid_referendum_valid(self):
        block_controller = BlockController()
        block_controller.add(Referendum("ref","pants",get_test_key()))

    def test_is_invalid_referendum_invalid(self):
        block_controller = BlockController()
        ref = Referendum("ref","pants",get_test_key())
        ref.creator_key = get_secondary_test_key().get_verifying_key()
        with self.assertRaises(BadSignatureError):
            block_controller.add(ref)

    def test_is_valid_vote_valid(self):
        block_controller = BlockController()
        ref = Referendum("ref", "pants", get_test_key())
        vote = Vote(ref.id,True,get_test_key())
        block_controller.add(ref)
        block_controller.mine_next_block() # before the ref is actually added to a block it is not referenceable
        block_controller.add(vote)


    def test_is_vote_without_ref_invalid(self):
        block_controller = BlockController()
        ref = Referendum("ref", "pants", get_test_key())
        vote = Vote(ref.id,True,get_test_key())
        with self.assertRaises(ReferendumNotFoundException):
            block_controller.add(vote)


    def test_double_vote_is_invalid(self):
        block_controller = BlockController()
        ref = Referendum("ref", "pants", get_test_key())
        vote = Vote(ref.id,True,get_test_key())
        block_controller.add(ref)
        block_controller.mine_next_block()
        block_controller.add(vote)
        block_controller.mine_next_block()
        with self.assertRaises(DoubleVoteException):
            block_controller.add(vote)



