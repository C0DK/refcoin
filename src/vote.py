from typing import Dict, Any

from ecdsa import SigningKey

from src.iblock_data_item import IBlockDataItem


class Vote(IBlockDataItem):
    def __init__(self, referendum_id: str, value: bool, creator_key : SigningKey) -> None:
        self.referendum_id = referendum_id
        self.value = value

        super().__init__(f"{referendum_id}{value}",creator_key)

    def to_json(self) -> Dict[str,Any]:
        return {**super().to_json(),"ref_id":self.referendum_id,"value":self.value}