from typing import List, cast

import time

from src.block import Block
from src.blockchain import Blockchain
from src.iblock_data_item import IBlockDataItem
from src.merkle_tree import MerkleTree
from src.referendum import Referendum
from src.vote import Vote

BLOCK_SIZE = 5

class NewChainIsShorterThanOldException(Exception): pass


class ElementInvalidException(Exception):
    pass


class DoubleVoteException(Exception):
    pass


class ReferendumNotFoundException(Exception):
    pass


class BlockController:
    def __init__(self, blocks: List[Block] = None) -> None:
        self.blockchain = Blockchain()
        self.miner_stop_flag = False
        self.merkle_tree: MerkleTree = MerkleTree([])

        if blocks is None:
            blocks = []
        self.set_chain(blocks)

    def set_chain(self, blocks: List[Block]):
        if len(self.blockchain) > len(blocks):
            raise NewChainIsShorterThanOldException()
        self.blockchain = Blockchain(blocks)

    def mine_next_block(self) -> Block:
        """
        Halts till the next block is mined, with valid data etc.
        :return: the next valid block
        """
        i = 0

        current_time = 0
        while not Block.is_hash_valid(Block.generate_hash(len(self.blockchain),
                                                          current_time,
                                                          self.merkle_tree.root,
                                                          i,
                                                          self.blockchain.last_hash())):
            current_time = int(time.time())
            i += 1

        block = Block(len(self.blockchain),
                      current_time,
                      self.merkle_tree,
                      i,
                      self.blockchain.last_hash())

        self.blockchain.add(block)
        #new merkle tree
        self.merkle_tree = MerkleTree([])
        return block

    def continuously_mine(self):
        self.miner_stop_flag = False
        while not self.miner_stop_flag:
            self.mine_next_block()

    def is_new_vote_valid(self,vote:Vote) -> bool:

        blocks = reversed(self.blockchain.get_blocks())
        for block in blocks:
            for item in block.merkle_tree.items:
                if issubclass(Vote,type(item)):
                    vote_item = cast(Vote,item)
                    if vote_item.referendum_id == vote.referendum_id and vote_item.creator_key == vote.creator_key:
                        raise DoubleVoteException()

                if issubclass(Referendum,type(item)):
                    referendum_item = cast(Vote,item)
                    if referendum_item.id == vote.referendum_id:
                        return True

        raise ReferendumNotFoundException()

        #check if has already voted AND referendum is in previous block

    def is_new_referendum_valid(self,referendum:Referendum) -> bool:
        return True

    def is_new_element_valid(self,element:IBlockDataItem) -> bool:
        #raises if fails
        #move this to the initializer of said class maybe?
        element.creator_key.verify(element.signature,element.id.encode())

        if issubclass(Vote,type(element)):
            return self.is_new_vote_valid(cast(Vote,element))
        elif issubclass(Referendum,type(element)):
            return self.is_new_referendum_valid(cast(Referendum,element))
        else:
            raise NotImplementedError



    def add(self, block_data:IBlockDataItem):
        if not self.is_new_element_valid(block_data):
            raise ElementInvalidException()

        return self.merkle_tree.add(block_data)

    def find_longer_chains(self):
        raise NotImplementedError
