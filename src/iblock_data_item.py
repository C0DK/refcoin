from abc import ABC, abstractmethod
from typing import Any, Dict

from ecdsa import SigningKey, VerifyingKey

from src.jsonable import IJsonable
from src.utils import CryptoUtils

# TODO make sure this is abstract?
class IBlockDataItem(IJsonable,ABC):
    def __init__(self, pre_hash_data:str, creator_key:SigningKey) -> None:
        self.creator_key:VerifyingKey = creator_key.get_verifying_key()
        self.id = CryptoUtils.double_sha256(pre_hash_data+str(self.creator_key.to_string()))
        self.signature:bytes = creator_key.sign(self.id.encode('utf-8'))

    @abstractmethod
    def to_json(self) -> Dict[str,Any]:
        return {"id":self.id, "creator_key":self.creator_key.to_string(),"signature":self.signature}

