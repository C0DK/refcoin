from abc import ABC, abstractmethod


class IJsonable(ABC):

    @abstractmethod
    def to_json(self):
        pass
