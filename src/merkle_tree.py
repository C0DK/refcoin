from typing import List

from src.iblock_data_item import IBlockDataItem
from src.utils import CryptoUtils


class MerkleTree:
    def __init__(self,items:List[IBlockDataItem]) -> None:
        self._items = list(items)
        self._levels:List[List[str]] = []
        self._ready = False
        self.make_tree()

    @property
    def top_layer(self) -> List[str]:
        return self._levels[-1]

    def bottom_layer(self) -> List[str]:
        return self._levels[0]

    def reset_levels(self):
        self._levels = [[x.id for x in self._items], ]

    @property
    def root(self) -> str:
        return self.top_layer[0]

    @property
    def items(self):
        return list(self._items)

    def add(self,item:IBlockDataItem):
        self._items.append(item)
        self.make_tree()

    def concat(self,items:List[IBlockDataItem]):
        self._items += items
        self.make_tree()


    def make_tree(self):
        self._ready = False
        self.reset_levels()
        if len(self._items) == 0:
            self._levels[0] = [CryptoUtils.double_sha256('')]

        while len(self.top_layer) > 1:
            new_level =  []
            if len(self.top_layer)%2!=0:
                self.top_layer.append(self.top_layer[-1])

            for i in range(0,len(self.top_layer),2):
                new_level.append(CryptoUtils.double_sha256(self.top_layer[i]+self.top_layer[i+1]))

            self._levels.append(new_level)
        self._ready = True


    def get_proofs(self,index:int) -> List[List[str]]:
        proofs = []
        for level in self._levels[:-1]:

            # TODO Make sure works with 6 (because 6 -> 3 -> 2 -> 1)

            is_right_node = bool(index%2)
            sibling_index = index - 1 if is_right_node else index + 1
            sibling_val = level[sibling_index]
            val = [sibling_val,None]
            if not is_right_node:
                val = list(reversed(val))

            proofs.append(val)
            index = int(index / 2.)
        return proofs

    @staticmethod
    def validate_proofs(proofs:List[List[str]],txid:str, root:str) -> bool:

        newest_proof = txid
        for level in proofs:
            value = "".join( x if x is not None else newest_proof for x in level)
            newest_proof = CryptoUtils.double_sha256(value)
        return newest_proof == root
