from typing import Dict, Any

from src.iblock_data_item import IBlockDataItem


class Referendum(IBlockDataItem):

    def __init__(self, title, body,creator_key):
        self.body = body
        self.title = title

        super().__init__(title + body,creator_key)


    def to_json(self) -> Dict[str,Any]:
        return {**super().to_json(),"body":self.body,"title":self.title}