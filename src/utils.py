import hashlib


class CryptoUtils:

    @staticmethod
    def double_sha256(value: str) -> str:
        return hashlib.sha256(hashlib.sha256(value.encode("utf-8")).hexdigest().encode("utf-8")).hexdigest()
