from typing import List

from src.block import Block


class ChainInvalidException(Exception):
    pass


class Blockchain:
    """
    A chain of blocks with verification etc
    """

    def __init__(self, blocks: List[Block] = None) -> None:
        """
        Initialize a blockchain
        :type blocks: a list of blocks
        """
        if blocks is None:
            blocks = []
        self._blocks: List[Block] = []
        # Add each block, to manually verify each one
        for block in blocks:
            self.add(block)

    @property
    def head(self) -> Block:
        return self._blocks[-1]

    def last_hash(self) -> str:
        return self._blocks[-1].hash if len(self._blocks) > 0 else None

    def is_new_block_valid(self, block: Block) -> bool:
        """

        :param block: A block to test
        :return: bool representing whether the block is valid as a new HEAD
        """
        if len(self._blocks) > 0 and self._blocks[-1].hash != block.previous_hash:
            return False
        return True

    def get_blocks(self):
        return list(self._blocks)

    def add(self, block: Block):
        if not self.is_new_block_valid(block):
            raise ChainInvalidException

        self._blocks.append(block)

    def __len__(self) -> int:
        return len(self._blocks)
