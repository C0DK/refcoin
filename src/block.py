"""
A block in the blockchain.
This holds all the data of a given block.
"""
from src.merkle_tree import MerkleTree
from src.utils import CryptoUtils

class InvalidHashException(Exception):
    pass

class Block:
    DIFFICULTY = 2
    def __init__(self, index: int, current_time: int, merkle_tree: MerkleTree, proof: int, previous_hash: str) -> None:
        """

        :param index: index of the given block
        :param timestamp: timestamp of the creation of the block
        :param data: data included in the block
        :param previous_hash: the hash of the previous block
        """
        self.merkle_tree = merkle_tree
        self.index = index
        self.timestamp = current_time
        self.proof = proof
        self.previous_hash = previous_hash
        self.hash = self.generate_hash(self.index,self.timestamp,self.merkle_tree.root,self.proof,self.previous_hash)
        if not self.is_valid():
            raise InvalidHashException()

    @property
    def data(self):
        return list(self.merkle_tree.items)

    def __str__(self):
        return f"#{self.index} ts: {self.timestamp} proof: {self.proof} prev_hash: {self.previous_hash}"

    def is_valid(self) -> bool:
        return self.is_hash_valid(self.hash)

    @staticmethod
    def is_hash_valid(hash_value:str) -> bool:
        return hash_value[:Block.DIFFICULTY] == "0" * Block.DIFFICULTY

    @staticmethod
    def generate_hash(index: int, timestamp: int, merkle_root:str, proof: int, previous_hash: str) -> str:
        """
        Generates a hash based on the properties of the given block
        :return: str
        """
        return CryptoUtils.double_sha256(f"{index}{timestamp}{merkle_root}{proof}{previous_hash}")
